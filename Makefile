#
# Neon
# Copyright (C) 2018   REAL-TIME CONSULTING
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

# The PROJECTS variable contains paths to all projects.
PROJECTS += test
PROJECTS += templates/app_template

.PHONY: all
all:
	@for p in $(PROJECTS); do $(MAKE) -C $${p} all; done
 
.PHONY: clean
clean:
	@for p in $(PROJECTS); do $(MAKE) -C $${p} clean; done

.PHONY: distclean
distclean:
	@for p in $(PROJECTS); do $(MAKE) -C $${p} distclean; done

.PHONY: test
test: all
	@./test/generated/test.elf
