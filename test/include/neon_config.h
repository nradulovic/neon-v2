/*
 * Application configuration file for Neon.
 */

#ifndef NEON_CONFIG_H_
#define NEON_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef TEST_DISABLE_DEBUG
#define NCONFIG_ENABLE_DEBUG 1
#endif
#define NCONFIG_ENABLE_LOGGER 1

#ifdef __cplusplus
}
#endif

/** @} */
/** @} */
/*---------------------------------------------------------------------------*/
#endif /* NEON_CONFIG_H_ */
